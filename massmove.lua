local mq = require('mq')

local myClass = mq.TLO.Me.Class.ShortName()
local myName = string.lower(mq.TLO.Me.CleanName())
local stickDistance = 15  -- TODO make this configurable
local traveling = false
local moveType = 'nav'
local followID
local followName

local function stopAll()
	if mq.TLO.Stick.Active() then
		mq.cmd('/stick off')
	end
	if mq.TLO.Navigation.Active() then
		mq.cmd('/nav stop')
	end
end

local function TravelCtrl(action, name)
	if name ~= nil then
		followID = mq.TLO.Spawn('pc ='..name).ID()
	end
	if followID == 0 then
		return
	end

	followName = mq.TLO.Spawn('id '..followID).CleanName()
	if action then
		print(string.format('massmove: Travel ON [%s:%s]', moveType, followName))
		-- turning on travel
		-- pause any running macro
		if not mq.TLO.Macro.Paused() then
			mq.cmd('/squelch /mqp on')
		end
		if myClass=='BRD' then
			local selosGem = mq.TLO.Me.Gem([[Selo's Song of Travel]])()
			if selosGem == nil then
				-- default to gem 3 ... TODO make this configurable
				mq.cmd([[/memspell 3 "Selo's Song of Travel"]])
				-- wait for spell book to open
				mq.delay('1s', function () return mq.TLO.Window("SpellBookWnd").Open() end)
				-- wait for spell book to close
				mq.delay('6s', function () return mq.TLO.Window("SpellBookWnd").Open() end)
				-- check for spell now
				selosGem = mq.TLO.Me.Gem([[Selo's Song of Travel]])()
			end
			if selosGem ~= nil then
				mq.cmd('/twist hold '..selosGem)
			end
		end
		if mq.TLO.Me.Mount() ~= nil then
			mq.cmd('/dismount')
		end
		-- stop any current stick / nav so we don't fight
		stopAll()
		if moveType == 'stick' then
			mq.cmd('/squelch /stick uw '..stickDistance..' id '..followID)
		else
			mq.cmd('/squelch /nav id '..followID)
		end
		traveling = true
	else
		print('massmove: Travel OFF')
		-- turning off travel
		if myClass=='BRD' and mq.TLO.Me.BardSongPlaying() then
			mq.cmd('/twist stop')
		end
		stopAll()
		if mq.TLO.Macro.Paused() then
			mq.cmd('/squelch /mqp off')
		end
		traveling = false
	end
end

local function TravelCmd(command, method, name)
	if command == 'help' then
		print('massmove.lua v1')
		print('/travel [command] [method] [name]')
		print('all parameters are optional, with no params will simply toggle travel mode on/off')
		print('  command  on|off     turns travelmode on or off explicitly')
		print('  method  nav|stick  which method to use for travel')
		print('  name               name of the character to follow, defaults to current target')
		return
	end

	local action
	-- if no command supplied, but other params are, just switch modes / target, but don't toggle travel
	if command == 'nav' or command == 'stick' then
		-- no command supplied, shift parms
		name = method
		method = command
		action = traveling
	elseif command ~= 'on' and command ~= 'off' then
		-- looks like we just have a name, use it
		name = command
		action = not traveling
	else
		action = (command == 'on')
	end
	
	if method ~= nil then
		if method == 'stick' or method == 'nav' then
			moveType = method
		else
			name = method
		end
	end
	
	if name == nil then
		-- attempt to use target
		if mq.TLO.Target.Type() == 'PC' and mq.TLO.Target.ID() ~= mq.TLO.Me.ID() then
			name = mq.TLO.Target.CleanName()
		end
	end
	
	TravelCtrl(action, name)
end

local function TravelMsg(line, sender, args_str)
	local args = {}
	for str in string.gmatch(args_str, "([^%s]+)") do
		table.insert(args, str)
	end
	
	table.insert(args, sender)
	
	TravelCmd(args[1], args[2], args[3])
end

-- for the /bc variant, which echos on all characters, check if its
-- to us before acting on it
local function TravelMsgB(line, sender, target, args_str)
	if target:lower() == myName then
		TravelMsg(line, sender, args_str)
	end
end

-- setup my events
mq.event('travel', '<#1#> #2# travel#3#', TravelMsgB)
mq.event('travelt', '[#1#(msg)] travel#2#', TravelMsg)

mq.bind('/travel', TravelCmd)

while not terminate
do
	mq.doevents()
	if traveling and followID ~= nil then
		-- check if followID still around or if we've zoned and their ID has changed
		local spawnID = mq.TLO.Spawn('pc ='..followName).ID()
		if spawnID ~= followID then
			if spawnID == 0 then
			--[[ our follow target doesn't exist anymore, for now, just idle and assume
				that the user will force a zone or turn off travel, or maybe the toon
				will return!
				TODO: set a timer / check if we're zoning, and if not, try to zone ourselves somehow
			]]--
			else
				-- found a new ID for our spawn, we probably just zoned or they left and came back
				-- store the new ID
				followID = spawnID
			end
		end
		-- are we still moving?
		if (moveType == 'nav' and not mq.TLO.Navigation.Active()) or (moveType == 'stick' and not mq.TLO.Stick.Active()) then
			-- restart the nav/stick command, but only if our follow target is in zone and has moved away
			if spawnID ~= 0 and mq.TLO.Spawn('id '..followID).Distance3D() > stickDistance then
				if moveType == 'stick' then
					mq.cmd('/squelch /stick uw '..stickDistance..' id '..followID)
				else
					mq.cmd('/squelch /nav id '..followID)
				end
			end
		end
	end
	mq.delay(10)
end