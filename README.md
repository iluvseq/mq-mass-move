# MQ Mass Move

Everquest / MQ LUA script to provide chase / tie functionality outside of a macro.

## Commands:

### Travel
    This is for when you want to move your crew through a zone without engaging.
    Issues an /mqp to pause any running macros
    Any character on a mount will /dismount
    Bards will sing Selo's Song of Travel

    Will use MQ2Nav /nav by default, but also supports MQ2MoveUtils /stick
    If 'stick' is used, will use the uw option so that toons will attempt to
    stay at the same height as the leader.  Useful when levitating or underwater

    /travel [command] [method] [name]
    all parameters are optional, with no params will simply toggle travel mode on/off

		command  on|off     turns travelmode on or off explicitly
		method  nav|stick   which method to use for travel
		name                name of the character to follow, defaults to current target
   
    Also can be triggered via EQBC, as simply 'travel' (no slash). Will default to
    following whomever sends the command.

    For example:
        /bca travel       all toons on your EQBC server will follow sender
        /bct Toon travel  "Toon" will follow sender
        /bcg travel       all toons in your group will follow sender
        /bca travel stick all toons will follow using /stick

    When turned off (either via toggle, or /travel off) will stop singing
    Selo's and will issue /mqp on to resume any macro that was running prior
    to /travel on
